const bodyParser = require('body-parser')
const compression = require('compression')
const cors = require('cors')
const express = require('express')
const {connectToMongoose} = require('./mongoose')
const passport = require('passport')

module.exports = {
  createExpress: (routes) => {
    const app = express()
    app.use(passport.initialize({}))
    connectToMongoose()

    app.use(cors())
    app.use(compression())

    app.use(bodyParser.urlencoded({extended: false}))
    app.use(bodyParser.json())
    app.use(routes)

    return app
  }
}
