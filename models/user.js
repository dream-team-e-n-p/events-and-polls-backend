const mongoose = require('mongoose')

const {USER} = require('./refs')

const userSchema = new mongoose.Schema({
  _id: {type: mongoose.Schema.Types.ObjectId, default: mongoose.Types.ObjectId()},
  firstName: {type: String, default: ''},
  lastName: {type: String, default: ''},
  email: {type: String, required: [true, 'E-Mail is required'], index: {unique: true}},
  password: {type: String, required: [true, 'Password is required']},
  timestamp: {type: Date, default: Date.now}
})

module.exports = mongoose.model(USER, userSchema)
