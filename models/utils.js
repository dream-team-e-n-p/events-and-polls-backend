module.exports = {
  isValidModelId: function (data) {
    const idRegexp = new RegExp("^[a-fA-F0-9]{24}$")
    return data && typeof data === "string" && idRegexp.test(data)
  }
}
