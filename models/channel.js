const mongoose = require('mongoose')

const {CHANNEL} = require('./refs')

const channelSchema = new mongoose.Schema({
  _id: {type: mongoose.Schema.Types.ObjectId, default: mongoose.Types.ObjectId()},
  name: {type: String, default: ''},
  timestamp: {type: Date, default: Date.now},
  users: [mongoose.Schema.Types.ObjectId]
})

module.exports = mongoose.model(CHANNEL, channelSchema)
