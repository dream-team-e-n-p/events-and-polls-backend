const mongoose = require('mongoose')

const MONGODB_URL = loadMongoDBURL()
console.log('Using: ' + MONGODB_URL)

function loadMongoDBURL() {
  return process.env.MONGODB_URL
    ? process.env.MONGODB_URL
    : 'mongodb://localhost/events-and-polls' + (process.env.NODE_ENV === 'test' ? '-test' : '')
}

module.exports = {
  connectToMongoose: () => {
    const mongooseConnection = mongoose.connect(MONGODB_URL, {useNewUrlParser: true})

    mongooseConnection
      .then(() => {
        const db = mongoose.connection
        db.on('error', console.error.bind(console, 'MongoDB connection error:'))
      })
      .catch((e) => {
        console.error(`MongoDB connection error: ${e}`)
      })
  }
}
