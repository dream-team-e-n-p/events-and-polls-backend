const {Channel} = require('../../models')
const {isValidModelId} = require('../../models/utils')
const {Router} = require('express')
const router = new Router()

const validateChannel = (req, res, next) => {
  if (isValidModelId(req.params.id)) {
    Channel.findById(req.params.id)
      .then(channel => {
        if (channel !== null) {
          if (channel.users.includes(req.user._id)) {
            req.channel = channel
            next()
          } else {
            return res.status(403).json({
              errors: {
                message: "You don't belong to this channel",
              },
            })
          }
        } else {
          return res.status(404).json({
            errors: {
              message: "Channel with the provided id is not found",
            },
          })
        }
      }).catch((err) => {
      return res.status(500).json({
        errors: {
          message: err.message,
        },
      })
    })
  } else {
    return res.status(404).json({errors: {message: "The provided domain id is not valid"}})
  }
}

const channelMembersMiddlewares = [validateChannel]

// @route   GET api/channels/:id
// @desc    Get channel by id
// @access  Private
router.get("/:id", channelMembersMiddlewares, (req, res) => {
  res.json(req.channel)
})

module.exports = router
