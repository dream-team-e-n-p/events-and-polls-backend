const {Router} = require('express')
const passport = require('passport')
require('./passport')(passport)
const auth = require('./auth')
const channels = require('./channels')

const router = new Router()
const authenticate = passport.authenticate('jwt', {session: false})

router.use('/api', auth)
router.use('/api/channels', authenticate, channels)

module.exports = router
