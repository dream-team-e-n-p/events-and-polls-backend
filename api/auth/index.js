const {Router} = require('express')
const {Channel, User} = require('../../models')
const jwt = require('jsonwebtoken')
const router = new Router()

// @route   POST api/register
// @desc    Register a new user
// @access  Public
router.post('/register', function (req, res) {
  const user = createUser(req)
  Channel.findOne({}, function (err, defaultChannel) {
    let channel = defaultChannel

    if (channel === null) {
      channel = createDefaultChannel()
    }
    channel.users.push(user.id)

    user
      .save()
      .then(() => {
        channel
          .save()
          .then(() => {
            res.json({user})
          })
      })
      .catch(err => {
        let errors = {}

        const MONGO_UNIQUE_CONSTRAINT_VIOLATION_CODE = 11000
        if (err.code && err.code === MONGO_UNIQUE_CONSTRAINT_VIOLATION_CODE) {
          errors.email = 'User with this Email is already exists'
        } else {
          errors.other = err
        }
        res.status(400).json({errors: errors})
      })
  })
})

// @route   POST api/login
// @desc    User login
// @access  Public
router.post('/login', function (req, res) {
  const errors = {}

  User.findOne({email: req.body.email})
    .then(user => {
      if (!user) {
        errors.email = 'Incorrect login'
        return res.status(404).json({errors: errors})
      } else {
        const matches = user.password === req.body.password
        if (matches) {
          const payload = {id: user._id}
          jwt.sign(
            payload,
            process.env.SECRET,
            {expiresIn: '10000h'},
            (err, token) => {
              res.json({
                token: 'Bearer ' + token,
                userId: user._id.toString(),
                userName: `${user.firstName.toString()} ${user.lastName.toString()}`,
              })
            })
        } else {
          errors.password = 'Incorrect password'
          return res.status(404).json({errors: errors})
        }
      }
    })
})

function createUser(req) {
  return new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password
  })
}

function createDefaultChannel() {
  return new Channel({
    name: 'Default',
    users: []
  })
}

module.exports = router
