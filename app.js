const api = require('./api')
const {createExpress} = require('./express')
const http = require('http')

run()

function run() {
  const app = createExpress(api)
  const server = http.createServer(app)

  const port = getPort()

  server.listen(port, () => {
    console.log('Express server listening on port %s', port)
  })
}

function getPort() {
  return (process.env.PORT || '5000')
}
